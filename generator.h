//================================
// Prototype Functions
//================================
char *generateAA2F( char *word,  char *result, STACKDATA *stk, int patternVectors[][CARDINALITY] );
bool isAA2FFullCheck ( char *word, int patternVectors[][CARDINALITY]  );
bool isAA2FPartialCheck ( char *word, int patternVectors[][CARDINALITY]  );
void createListOfParikhVector(char *word, long listOfParikhVector[][CARDINALITY] );
void parikhCalc ( int firstIndex, int length, int *result, long listOfParikhVector[][CARDINALITY]);
bool confirmAA2F( int *result1, int *result2 );
bool confirmPatternVec( char *word, int patternVectors[][CARDINALITY], long listOfParikhVector[][CARDINALITY]);
int returnSumOfComponents( int *array );

void showList(char *word, long listOfParikhVector[][CARDINALITY] );
void initList(char *word, long listOfParikhVector[][CARDINALITY] );
void showArray( int *array );
void initArray( int *array );
int pop ( STACKDATA *stk, char *pop_data );
int push ( STACKDATA *stk, char *push_data );
void stackPrint( STACKDATA *stk);
void printWord ( char *word );
