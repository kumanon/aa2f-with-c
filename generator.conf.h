#define CARDINALITY 3
#define MAXWORDLENGTH 13000 // max word length
#define STACK_SIZE 13000
#define SUCCESS 1
#define FAILURE 0
#define QSIZE   100  // size of message queue
#define NUMBEROFTHREADS 16 // number of threads
#define NUMBEROFPATTERNVECTORS 1 // number of pattern
#define FIRST_LETTER_OF_ALPHABET 97 // it means character 'a'

typedef enum {FALSE, TRUE} bool;

typedef struct stack_data {
  int num;
  unsigned char word[STACK_SIZE][MAXWORDLENGTH];

} STACKDATA;
