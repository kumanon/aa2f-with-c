// Author:Keima Kumano
// Update:2015/01/12
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include "generator.conf.h"
#include "generator.h"

//================================
// Main Process
//================================
int main( int argc, char*argv[] ){
  // initialize
  unsigned char result[MAXWORDLENGTH];
  unsigned char word[MAXWORDLENGTH] = "";
  STACKDATA *stk;
  int wordLength = 0;
  int n;
  int patternVectors[NUMBEROFPATTERNVECTORS][CARDINALITY] = {{3,0,0}};   // set pattern vector
  
  // dynamic memory allocation (not yet complete)
  stk = (STACKDATA*) malloc( sizeof(STACKDATA));
  stk -> num = 0;
  
  // preparation of parallelization
  #if _OpenMP
  n = omp_get_max_threads(); // get default number of threads
  printf("max threads (default): %d\n",n);

  omp_set_num_threads(NUMBEROFTHREADS);   // change number of threads
  n = omp_get_max_threads(); 
  printf("max threads (set): %d\n",n);
  #endif
  
  // start time measurement
  clock_t  start = clock();
  
  while( 1 ) {
    // for stack
    if ( stk->num != 0 ) { 
      pop(stk, word);
    } else if ( stk->num >= STACK_SIZE ) {
      printf("STACK OVER FLOW\n");
      break;
    } 
    //printf("%d ", stk->num);
    // generate AA2F word
    generateAA2F(word, result, stk, patternVectors);
    
    // print AA2F words, if it has the longest length
    if ( wordLength < strlen(result) ) {
      printWord(result); 
      wordLength = strlen(result);
      // end time mesurement and print measured time with word length
      if ( wordLength % 50 == 0 ) {
	clock_t end = clock();
	//printf("%d,%f\n",wordLength,(double)(end-start) / CLOCKS_PER_SEC);
      }
    }
  }
  free(stk);
  return 0;
}

char *generateAA2F( char *word,  char *result, STACKDATA *stk, int patternVectors[][CARDINALITY] ) {
  char str[strlen(word)];
  char aCode = 'a';
  char tmp;
  int i;
  
  for ( i = 0; i < CARDINALITY; i++ ) {
    tmp = aCode + i;
    sprintf(str,"%s%c",word, tmp);
    if ( isAA2FPartialCheck( str, patternVectors ) ) {
      push(stk, str);
      strcpy(result, str);
    }
  }
  return result;
}

bool isAA2FFullCheck ( char *word, int patternVectors[][CARDINALITY]  ) {
  // list of letter counts
  long listOfParikhVector[strlen(word)][CARDINALITY]; 
  long i,j;
  int result1[CARDINALITY];
  int result2[CARDINALITY];
  
  // do once each words
  createListOfParikhVector (word, listOfParikhVector);

  // compare
  for ( i = 0; i < strlen(word) - 3; i++ ) {
    for ( j = 2; j <= (strlen(word)-i) / 2; j++ ) {
      parikhCalc(i, j, result1, listOfParikhVector);
      parikhCalc(i + j, j, result2, listOfParikhVector);
      if ( confirmAA2F( result1, result2 ) == FALSE ) {
	return FALSE;
      }
    }
  }
  return TRUE;
}

bool isAA2FPartialCheck( char *word, int patternVectors[][CARDINALITY] ) {
  // list of letter counts
  long listOfParikhVector[strlen(word)][CARDINALITY]; 
  long i;
  long factorLen = 0;
  int result1[CARDINALITY];
  int result2[CARDINALITY];
  bool returnBoolFlag = FALSE;
 
  // do once each words
  createListOfParikhVector (word, listOfParikhVector);
  
  // compare
  for ( i = strlen(word) - 4; i >= 0; i -= 2 ) {
    factorLen = (strlen(word) - i) / 2;
    parikhCalc(i, factorLen, result1, listOfParikhVector);
    parikhCalc(i + factorLen, factorLen, result2, listOfParikhVector);
    // 1. confirm vec(v) = vec(u)
    if ( confirmAA2F( result1, result2  ) == FALSE ) {
	return FALSE;
    } else {
      // 2. confirm vec(v) - vec(u) = vec(p) 
      if ( confirmPatternVec( word, patternVectors, listOfParikhVector ) == FALSE ) {
      	return FALSE;
      }
    }
  }

  return TRUE;
}

void createListOfParikhVector ( char *word, long listOfParikhVector[][CARDINALITY] ) {
  long i,j;
  initList(word, listOfParikhVector);
  // calculation using multi-thread
  #if _OpenMP
  #pragma omp parallel for private(j)
  #endif
  for ( i = 1; i <= strlen(word); i++ ) {
    for ( j = 0; j < i; j++ ) {
      listOfParikhVector[i][word[j] - FIRST_LETTER_OF_ALPHABET]++;
    }
  }
}

void parikhCalc( int firstIndex, int length, int *result, long listOfParikhVector[][CARDINALITY]) {
  long i;
  int startVector[CARDINALITY];
  int endVector[CARDINALITY];
  initArray(result);
  for ( i = 0; i < CARDINALITY; i++ ) {
    startVector[i] = listOfParikhVector[firstIndex][i];
    endVector[i] = listOfParikhVector[firstIndex + length][i];  
    result[i] = endVector[i] - startVector[i];
  }
}

bool confirmAA2F ( int *result1, int *result2 ) {
  int i;  
  for ( i = 0; i < CARDINALITY-1; i++ ) {
    if ( result1[i] != result2[i] ) {
      return TRUE;
    }
  }
  return FALSE;
}

bool confirmPatternVec ( char *word, int patternVectors[][CARDINALITY], long listOfParikhVector[][CARDINALITY] ) {
  int i,j,k;
  int vec_v[CARDINALITY];
  int vec_u[CARDINALITY];
  
  // confirm vec(v) - vec(u) == vec(p) using pattern vectors
  // this loop for confirming all pattern vectors
  for ( i = 0; i < NUMBEROFPATTERNVECTORS; i++ ) {
    // initialize
    parikhCalc((strlen(word) - returnSumOfComponents(patternVectors[i])), returnSumOfComponents(patternVectors[i]), vec_v, listOfParikhVector );
    for ( j = 0; j < CARDINALITY; j++ ) {
      vec_u[j] = 0;
    }
    for ( k = 0; (strlen(word) - (returnSumOfComponents(vec_v) + returnSumOfComponents(vec_u))) > 1; k++ ) {
      int numberOfSameComponents = 0;
      // calc
      parikhCalc((strlen(word) - returnSumOfComponents(patternVectors[i])) - k, returnSumOfComponents(patternVectors[i]) + k, vec_v, listOfParikhVector );
      parikhCalc(strlen(word) - k,  k, vec_u, listOfParikhVector );
      for ( j = 0; j < CARDINALITY-1; j++ ) {
	if ( vec_v[j] - vec_u[j] == patternVectors[i][j]  ) {
	  numberOfSameComponents++;
	}
      }
      // if it includes pattern vector, then return FALSE
      if ( numberOfSameComponents == CARDINALITY-1 ) {
	return FALSE;
      }
    }
  }
  return TRUE;
}

void printWord( char *word ) {
  printf("%d %s\n", strlen(word), word);
}

int returnSumOfComponents ( int *array ) {
  int i;
  int sum = 0;
  for ( i = 0; i < CARDINALITY; i++ ) {
    if ( array[i] < 0 ) {
      sum += array[i] * -1;
    } else {
      sum += array[i];
    }
  }
  return sum;
}
//===========================
// Stack Function
//===========================
int push ( STACKDATA *stk, char *push_data ) {
  if ( stk -> num < STACK_SIZE ) {
    strcpy(stk -> word[stk->num],push_data);
    //printf("%s", stk -> word[stk -> num]);
    stk -> num++;
    return SUCCESS;
  } else {
    return FAILURE;
  }
}

int pop ( STACKDATA *stk, char *pop_data ) {
  if ( stk -> num > 0 ) {
    stk -> num--;
    strcpy(pop_data,stk -> word[stk->num]);
    return SUCCESS;
  } else {
    return FAILURE;
  }
}

void stackPrint( STACKDATA *stk ) {
  int i;
  printf("stack [");
  for ( i = 0; i < stk -> num; i++ ) {
    printf("%s", stk -> word[i]);
  }
  printf("]\n");
}

//===========================
// Initialize Function
//===========================
void initList ( char *word, long listOfParikhVector[][CARDINALITY] ) {
  long i;
  int j;
  for ( i = 0; i <= strlen(word); i++ ) {
    for ( j = 0; j < CARDINALITY; j++ ) {
      listOfParikhVector[i][j] = 0;
    }
  } 
}

void initArray ( int *array ) {
  long i;
  //  for ( i = 0; i <= sizeof(array) / sizeof(array[0]) ; i++ ) {
  for ( i = 0; i <CARDINALITY ; i++ ) {
    array[i] = 0;
  }
}

//===========================
// Debug Function
//===========================

void showArray( int *array ) {
  long i;
  for ( i = 0; i < CARDINALITY; i++ ) {
    printf("%d ", array[i]);
  }
  printf("\n");
}

void showList ( char *word, long listOfParikhVector[][CARDINALITY] ) {
  long i;
  int j;
  for ( i = 0; i <= strlen(word); i++ ) {
    printf("[");
    for ( j = 0; j < CARDINALITY; j++ ) {
      printf ("%d ", listOfParikhVector[i][j]);
    }
    printf("]\n");
  } 
}
